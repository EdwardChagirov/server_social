const express = require('express')
const router = express.Router()

const userControllers = require('../controllers/userControllers')

router.post('/reg', userControllers.userReg)

router.post('/auth', userControllers.userAuth)

module.exports = router
