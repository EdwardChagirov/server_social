'use strict'
module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define(
    'User',
    {
      login: {
        type: Sequelize.STRING,
        validate: {
          is: {
            args: /^\w+$/g,
            msg: 'Доступные символы: a-z, A-Z, 0-9, _',
          },
          len: {
            args: 6,
            msg: 'Минимум из 6 символов',
          },
        },
      },
      password: {
        type: Sequelize.STRING,
        validate: {
          is: {
            args: /^[a-zA-Z0-9&$?^\-_.]+$/g,
            msg: 'Доступные символы: a-z, A-Z, 0-9, & $ ? ^ - . _',
          },
          len: {
            args: 6,
            msg: 'Пароль должен быть минимум из 6 символов',
          },
        },
      },
      name: Sequelize.STRING,
      phone: Sequelize.STRING,
      age: Sequelize.INTEGER,
      email: Sequelize.STRING,
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
    },
    {}
  )
  // eslint-disable-next-line no-unused-vars
  User.associate = function(models) {
    // associations can be defined here
  }
  return User
}
