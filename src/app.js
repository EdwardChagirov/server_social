const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const compression = require('compression')
const helmet = require('helmet')
const createError = require('http-errors')
const indexRouter = require('./routes/index')
const userRouter = require('./routes/user')
const app = express()
const _ = require('lodash')

app.use(helmet())
app.use(compression())

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/', indexRouter)
app.use('/user', userRouter)

app.use(function(req, res, next) {
  next(createError(404))
})

// eslint-disable-next-line no-unused-vars
app.use(function(err, req, res, next) {
  let errors = _(err.errors || err).map((error, index) => ({
    id: index,
    message: error.message,
    path: error.path,
  }))

  res.status(200).json({ errors })
})

module.exports = app
