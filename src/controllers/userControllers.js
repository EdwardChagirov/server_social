const User = require('../db').User

exports.userReg = (req, res, next) => {
  User.create({ login: req.body.login, password: req.body.password })
    .then(user => {
      res.status(200).json(user)
    })
    .catch(err => {
      next(err)
    })
}

exports.userAuth = (req, res, next) => {
  User.findOne({ where: { login: req.body.login } })
    .then(user => {
      if (!user) throw new Error('Пользователь не найден')
      res.status(200).json(user)
    })
    .catch(err => {
      let error = {
        message: err.message,
        path: 'auth',
      }
      next([error])
    })
}
