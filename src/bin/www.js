const app = require('../app')
const db = require('../db')

app.set('port', 3030)

db.sequelize.sync().done(() => {
  app.listen(app.get('port'), () => console.log(`Server start on port ${app.get('port')}`))
})
